package com.kabigon.guitarchord;

import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.Date;

/**
 * Created by Kabi on 8/26/16.
 */
public class MyApplication extends Application {

    public boolean wasInBackground;

    @Override
    public void onTrimMemory(final int level) {
        if (level == ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN) {
            // Get called every-time when application went to background.
            this.wasInBackground = true;
        }


    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

    }


    // Analytics
    public static Tracker mTracker;

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker("UA-68870820-3");
        }
        return mTracker;
    }

    public static void trackerScreen(String screenName){
        if(mTracker != null){
            mTracker.setScreenName(screenName);
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        }
    }




}
