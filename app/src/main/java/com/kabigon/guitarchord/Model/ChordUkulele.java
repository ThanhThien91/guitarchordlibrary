package com.kabigon.guitarchord.Model;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by Kabi on 7/29/16.
 */
public class ChordUkulele extends RealmObject {

    @Required
    @PrimaryKey
    public String chord_id = "";

    @Required
    public String name = "";

    public int priority = 100;

    @Required
    public String fingers = "";

    @Required
    public String scheme = "";

    @Required
    public String category = "";

    public static RealmList<ChordUkulele> queryChordWithChordName(String chordName)
    {
        String text = chordName;
        text = text.replace("Db", "C#");
        text = text.replace("D#", "Eb");
        text = text.replace("Gb", "F#");
        text = text.replace("G#", "Ab");
        text = text.replace("A#", "Bb");

        Realm realm = Realm.getDefaultInstance();
        RealmQuery query = realm.where(ChordUkulele.class).equalTo("name", text, Case.INSENSITIVE);
        RealmResults<ChordUkulele> results = query.findAll();

        RealmList <ChordUkulele> listChordUkulele = new RealmList<>();
        listChordUkulele.addAll(results);
        return listChordUkulele;
    }


    public static RealmList<ChordUkulele> getChordByCategoryName(String categoryName)
    {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery query = realm.where(ChordUkulele.class);
        query.equalTo("category",categoryName,Case.INSENSITIVE);
        RealmResults<ChordUkulele> results = query.findAll();
        RealmList <ChordUkulele> listChordUkulele = new RealmList<>();
        listChordUkulele.addAll(results);
        return listChordUkulele;
    }

    public static RealmList<ChordUkulele> suggestChordResultSearch(String textSearch)
    {
        String text = textSearch;
        text = text.replace("Db","C#");
        text = text.replace("D#","Eb");
        text = text.replace("Gb","F#");
        text = text.replace("G#","Ab");
        text = text.replace("A#","Bb");

        Realm realm = Realm.getDefaultInstance();
        RealmQuery query = realm.where(ChordUkulele.class);
        query.beginsWith("name",text,Case.INSENSITIVE);
        RealmResults<ChordUkulele> results = query.findAll().sort("name");
        RealmList <ChordUkulele> listChordUkulele = new RealmList<>();
        listChordUkulele.addAll(results);
        return listChordUkulele;
    }


    public static RealmList<ChordUkulele> searchChord(String textSearch)
    {
        String text = textSearch;
        text = text.replace("Db", "C#");
        text = text.replace("D#", "Eb");
        text = text.replace("Gb", "F#");
        text = text.replace("G#", "Ab");
        text = text.replace("A#", "Bb");

        Realm realm = Realm.getDefaultInstance();
        RealmQuery query = realm.where(ChordUkulele.class);
        query.equalTo("name",text,Case.INSENSITIVE);
        RealmResults<ChordUkulele> results = query.findAll().sort("name");
        RealmList <ChordUkulele> listChordUkulele = new RealmList<>();
        listChordUkulele.addAll(results);
        return listChordUkulele;
    }
}
