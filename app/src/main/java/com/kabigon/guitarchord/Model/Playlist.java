package com.kabigon.guitarchord.Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;
import com.kabigon.guitarchord.Common.Utilities;

/**
 * Created by Kabi on 8/19/16.
 */
public class Playlist extends RealmObject {

    @PrimaryKey
    public int playlistId = 1;

    @Required
    public String title = "";

    @Required
    public String content = "";

    @Required
    public String createdDate = Utilities.DateToString(new Date());

    public RealmList<Playlist_Song> listSongId = new RealmList<>();

    /********************************************************************************/
    // MARK: - Query


    public static RealmList<Playlist> getAllPlaylist()
    {
        // Get the default Realm
        Realm realm = Realm.getDefaultInstance();

        // Query using a predicate string
        RealmQuery query = realm.where(Playlist.class);
        RealmResults<Playlist> results = query.findAll();

        // Add to the Realm inside a transaction
        realm.beginTransaction();
        for(Playlist item : results){
            RealmQuery querySongInPlaylist = realm.where(Playlist_Song.class).equalTo("playlistId", item.playlistId);
            RealmResults<Playlist_Song> resultsSongInPlaylist = querySongInPlaylist.findAll();
            item.listSongId.clear();
            item.listSongId.addAll(resultsSongInPlaylist);
        }
        realm.commitTransaction();


        RealmList <Playlist> listPlaylist = new RealmList<>();
        listPlaylist.addAll(results);
        return listPlaylist;
    }



    public static int getNumberPlaylist()
    {
        // Get the default Realm
        Realm realm = Realm.getDefaultInstance();

        // Query using a predicate string
        RealmQuery query = realm.where(Playlist.class);
        RealmResults<Playlist> results = query.findAll();

        return results.size();
    }



    public static ArrayList<String> getListSongInPlaylistWithPlaylistId(int playlistId)
    {
        // Get the default Realm
        Realm realm = Realm.getDefaultInstance();

        // Query using a predicate string
        RealmQuery query = realm.where(Playlist_Song.class).equalTo("playlistId", playlistId);
        RealmResults<Playlist_Song> results = query.findAll().sort("dateAdd", Sort.DESCENDING);

        ArrayList<String> listSongId = new ArrayList<>();
        for(Playlist_Song item : results)
        {
            listSongId.add(item.songId);
        }

        return listSongId;
    }



    public static Playlist getPlaylistWithId(String playlistId)
    {
        // Get the default Realm
        Realm realm = Realm.getDefaultInstance();

        // Query using a predicate string
        RealmQuery query = realm.where(Playlist.class).equalTo("playlistId", playlistId);
        RealmResults<Playlist> results = query.findAll();

        return results.size() != 0 ? results.first() : null;
    }



    /********************************************************************************/


    /********************************************************************************/
    // MARK: - Insert row


//    public static syncPlaylist(listPlaylist : [NSDictionary])
//    {
//        do{
//            // Get the default Realm
//            let realm = try Realm()
//
//            try realm.write(){
//                // sync to db
//                for item in listPlaylist
//                {
//                    // get playlistId
//                    if let playlistId = item["playlist_id"] as? String
//                    {
//                        let title = item["name"] as! String
//                        let content = item["description"] as! String
//                        let createdDate = item["date"] as! String
//
//
//                        // Create a Playlist object
//                        let object = Playlist()
//                        object.title = title
//                        object.content = content
//                        object.playlistId = Int(playlistId)!
////                        object.rowId = NSUUID().UUIDString
//
//                        if createdDate.characters.count != 0
//                        {
//                            object.createdDate = createdDate
//                        }
//
//                        // insert to db
//                        realm.add(object)
//
//                        // add list song to playlist
//                        if let listSongId = item["song_ids"] as? [String]
//                        {
//                            if listSongId.count != 0
//                            {
//                                Playlist_Song.addListSongToPlaylist(object.playlistId, listSongId: listSongId,realm : realm)
//                            }
//                        }
//                    }
//
//                }
//            }
//        }
//        catch let error as NSError{
//        // handle error
//        print("---------------error syncPlaylist : \(error)--------------")
//    }
//    }


    public static int createPlaylist(String title,String content,int playlistId,String createdDate,Realm realm)
    {
        // Create a Playlist object
        Playlist object = new Playlist();
        object.title = title;
        object.content = content;

        Number value = realm.where(Playlist.class).max("playlistId");
        if(value != null){
            int maxValue = value.intValue();
            object.playlistId = maxValue + 1;
        }
        else{
            object.playlistId = 1;
        }



        if(createdDate.length() != 0)
        {
            object.createdDate = createdDate;
        }

        // insert to db
        realm.copyToRealm(object);

        return object.playlistId;

    }

    /********************************************************************************/



    /********************************************************************************/
    // MARK: - Update


    public static void updatePlaylist(String title ,String content,int playlistId,String createdDate,Playlist playlistItem,Realm realm)
    {
        // Update a Playlist object
        playlistItem.title = title;
        playlistItem.content = content;
//        playlistItem.playlistId = playlistId

        if(createdDate.length() != 0)
        {
            playlistItem.createdDate = createdDate;
        }


        // update
        realm.copyToRealmOrUpdate(playlistItem);

    }


    /********************************************************************************/


    /********************************************************************************/
    // MARK: - Delete


    public static void deletePlaylistWithId(int playlistId)
    {
        // Get the default Realm
        Realm realm = Realm.getDefaultInstance();

        // Query using a predicate string
        RealmQuery query = realm.where(Playlist.class).equalTo("playlistId", playlistId);
        RealmResults<Playlist> results = query.findAll();

        excuteDeletePlaylistAndListSongInPlaylist(realm,results);
    }



    public static void excuteDeletePlaylistAndListSongInPlaylist(Realm realm,RealmResults<Playlist> results)
    {
        if(realm.isInTransaction()){
            for(Playlist item : results){
                // delete all song in playlist
                Playlist_Song.deleteAllSongInPlaylist(item.playlistId,realm);

                // delete playlist
                item.deleteFromRealm();
            }
        }
        else{
            // Delete an object with a transaction
            realm.beginTransaction();
            for(Playlist item : results){
                // delete all song in playlist
                Playlist_Song.deleteAllSongInPlaylist(item.playlistId,realm);

                // delete playlist
                item.deleteFromRealm();
            }
            realm.commitTransaction();
        }

    }


    public static void deletePlaylistWithPlaylistId(int playlistId)
    {
        // Get the default Realm
        Realm realm = Realm.getDefaultInstance();

        // Query using a predicate string
        RealmQuery query = realm.where(Playlist.class).equalTo("playlistId", playlistId);
        RealmResults<Playlist> results = query.findAll();

        excuteDeletePlaylistAndListSongInPlaylist(realm,results);
    }



    public static void deletePlaylistSyncFromServer(int playlistId)
    {
        // Get the default Realm
        Realm realm = Realm.getDefaultInstance();

        // Query using a predicate string
        RealmQuery query = realm.where(Playlist.class).equalTo("playlistId", playlistId);
        RealmResults<Playlist> results = query.findAll();

        excuteDeletePlaylistAndListSongInPlaylist(realm,results);
    }




    public static void deleteAllPlaylist()
    {
        // Get the default Realm
        Realm realm = Realm.getDefaultInstance();

        // Query using a predicate string
        RealmQuery query = realm.where(Playlist.class);
        RealmResults<Playlist> results = query.findAll();

        excuteDeletePlaylistAndListSongInPlaylist(realm,results);

    }


    public static void syncPlaylist(String jsonString,Realm bgRealm)
    {
        try {
            final JSONArray jsonArray = new JSONArray(jsonString);
            if (jsonArray != null) {

                // remove current
                Playlist.deleteAllPlaylist();

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject item = jsonArray.getJSONObject(i);
                    // get playlistId
                    String playlistId = item.getString("playlist_id");
                    String title = item.getString("name");
                    String content = item.getString("description");
                    String createdDate = item.getString("date");

                    // Create a Playlist object
                    Playlist object = new Playlist();
                    object.title = title;
                    object.content = content;
                    object.playlistId = Integer.parseInt(playlistId);

                    if(createdDate.length() != 0)
                    {
                        object.createdDate = createdDate;
                    }

                    bgRealm.copyToRealm(object);

                    // add list song to playlist
                    ArrayList<String>  listSongId = new ArrayList<>();
                    JSONArray  listObject = item.getJSONArray("song_ids");
                    for(int j = 0; j < listObject.length(); j++) {
                        String songId = listObject.getString(j);
                        listSongId.add(songId);
                    }

                    if(listSongId.size() != 0)
                    {
                        Playlist_Song.addListSongToPlaylist(object.playlistId, listSongId,bgRealm);
                    }

                }
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }



    /********************************************************************************/



}
