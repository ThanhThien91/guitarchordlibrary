package com.kabigon.guitarchord.Model;

/**
 * Created by kabigon on 11/8/16.
 */

public class YoutubeVideo {
    public String videoId = "";
    public String title = "";
    public String description = "";
    public String thumbnail = "";
    public String channelTitle = "";
    public String views = "";
    public String duration = "";
}
