package com.kabigon.guitarchord.Model;

import android.content.Context;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;
import com.kabigon.guitarchord.Common.Utilities;

/**
 * Created by Kabi on 6/29/16.
 */
public class Chord extends RealmObject {

    @PrimaryKey
    @Required
    public String chord_id = "";

    @Required
    public String chord_name = "";

    @Required
    public String chord_relations = "";


    public static RealmList<Chord> getAllChordFromListChordId(ArrayList<String> listChordId)
    {
        Realm realm = Realm.getDefaultInstance();

        // Query using a predicate string
        RealmQuery query = realm.where(Chord.class);

        int i = 0;
        for (String chordId : listChordId) {
            // The or() operator requires left hand and right hand elements.
            // If articleIds had only one element then it would crash with
            // "Missing right-hand side of OR"
            if (i++ > 0) {
                query = query.or();
            }
            query = query.equalTo("chord_id", chordId);
        }

        RealmResults<Chord> results = query.findAll();
        RealmList <Chord> listChord = new RealmList<>();
        listChord.addAll(results);
        return listChord;
    }





    /********************************************************************************/
}
