package com.kabigon.guitarchord.Model;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by Kabi on 8/11/16.
 */
public class Song_Author extends RealmObject {

    @PrimaryKey
    public int row_id = 0;

    @Required
    public String artist_id = "";

    @Required
    public String song_id = "";


    public static Song_Author extractDataFromDictionary(JSONObject object, Realm realm, String songId){
        Song_Author song_author = new Song_Author();

        try {
            int maxValue = realm.where(Song_Author.class).max("row_id").intValue();
            song_author.row_id = maxValue + 1;

            if(object.has("artist_id")){
                String artistIdValue = object.getString("artist_id");
                song_author.artist_id = artistIdValue != null ? artistIdValue : "";
            }

            song_author.song_id = songId;

            return song_author;
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        return song_author;
    }


    /********************************************************************************/
    // MARK: - Query


    public static RealmList<Song_Author> getAuthorIdBySongId(String songId)
    {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery query = realm.where(Song_Author.class).equalTo("song_id", songId);
        RealmResults<Song_Author> results = query.findAll();
        RealmList<Song_Author> listSong_Author = new RealmList<>();
        listSong_Author.addAll(results);
        return listSong_Author;
    }


    public static RealmList<Song_Author> getListSongIdByArtistId(String artistId)
    {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery query = realm.where(Song_Author.class).equalTo("artist_id", artistId);
        RealmResults<Song_Author> results = query.findAll();
        RealmList<Song_Author> listSong_Author = new RealmList<>();
        listSong_Author.addAll(results);
        return listSong_Author;
    }

    /********************************************************************************/
}
