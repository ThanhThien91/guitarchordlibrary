package com.kabigon.guitarchord.Model;

import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;
import com.kabigon.guitarchord.Common.AppSetting;

/**
 * Created by kabigon on 1/3/17.
 */

public class RecentView extends RealmObject {

    @PrimaryKey
    @Required
    public String songId = "";

    @Required
    public Date lastView = new Date();


    public static void addSong(String songId){
        // Get the default Realm
        Realm realm = Realm.getDefaultInstance();

        // kiem tra keyword da duoc search chua co roi thi tang bien dem so lan search
        RealmQuery query = realm.where(RecentView.class);
        query.equalTo("songId",songId.trim());
        RealmResults<RecentView> results = query.findAll();
        if(results.size() != 0)
        {
            if(realm.isInTransaction())
            {
                saveResult(results,realm,songId);
            }
            else
            {
                realm.beginTransaction();
                saveResult(results,realm,songId);
                realm.commitTransaction();
            }

        }
        // add new keyword
        else
        {
            // Create a RecentView object
            RecentView recent = new RecentView();
            recent.songId = songId;
            recent.lastView = new Date();

            // You only need to do this once (per thread)

            // Add to the Realm inside a transaction
            if(realm.isInTransaction() == true)
            {
                realm.copyToRealm(recent);
            }
            else
            {
                realm.beginTransaction();
                realm.copyToRealm(recent);
                realm.commitTransaction();
            }

        }
    }


    private static void saveResult(RealmResults<RecentView> results, Realm realm, String songId){
        RecentView recent = results.first();
        recent.songId = songId;
        recent.lastView = new Date();
        realm.copyToRealmOrUpdate(recent);
    }


    public static List<RecentView> getListRecentView()
    {
        // Get the default Realm
        Realm realm = Realm.getDefaultInstance();
        RealmQuery query = realm.where(RecentView.class);

        RealmResults<RecentView> results = query.findAll().sort("lastView", Sort.DESCENDING);
        List<RecentView> listRecentView = new RealmList<>();

        // limit
        if(results.size() > AppSetting.kLimitNumberItemInRecentView)
        {

            listRecentView.addAll(results.subList(0,AppSetting.kLimitNumberItemInRecentView - 1));

            if(results.size() > AppSetting.kLimitNumberItemInRecentView + 100){
                realm.beginTransaction();
                for(int i = AppSetting.kLimitNumberItemInRecentView + 1; i < results.size(); i++){
                    RecentView recentView = results.get(i);
                    recentView.deleteFromRealm();
                }
                realm.commitTransaction();
            }

            return listRecentView;
        }
        else
        {
            listRecentView.addAll(results);
            return listRecentView;
        }
    }

}
