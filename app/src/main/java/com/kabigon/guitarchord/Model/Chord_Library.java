package com.kabigon.guitarchord.Model;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by Kabi on 6/30/16.
 */
public class Chord_Library extends RealmObject {

    @Required
    @PrimaryKey
    public String rowId = "";

    @Required
    public String name = "";

    public int priority = 0;

    @Required
    public String fingers = "";

    @Required
    public String scheme = "";


    public static RealmList<Chord_Library> getAllChord() {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery query = realm.where(Chord_Library.class);
        RealmResults<Chord_Library> results = query.findAll();

        RealmList <Chord_Library> listChordGuitar = new RealmList<>();
        listChordGuitar.addAll(results);

        return listChordGuitar;
    }


    public static RealmList<Chord_Library> getChordByChordId(String chordId) {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery query = realm.where(Chord_Library.class)
                                .equalTo("rowId", chordId);

        RealmResults<Chord_Library> results = query.findAll().sort("priority");

        RealmList <Chord_Library> listChordGuitar = new RealmList<>();
        listChordGuitar.addAll(results);
        return listChordGuitar;
    }

    public static RealmList<Chord_Library> getChordByName(String name) {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery query = realm.where(Chord_Library.class).equalTo("name", name, Case.INSENSITIVE);
        RealmResults<Chord_Library> results = query.findAll().sort("priority");

        RealmList<Chord_Library> listChordGuitar = new RealmList<>();
        listChordGuitar.addAll(results);
        return listChordGuitar;
    }


    public static Chord_Library getDefaultChordByName(String name) {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery query = realm.where(Chord_Library.class).equalTo("name", name, Case.INSENSITIVE);
        query.equalTo("priority",1);
        RealmResults<Chord_Library> results = query.findAll();

        if (results.size() == 0) {
            query = realm.where(Chord_Library.class).equalTo("name", name, Case.INSENSITIVE);
            results = query.findAll().sort("priority");
        }

        return results.size() != 0 ? results.first() : null;
    }

    static void filterWithCategoryName(RealmList<Chord_Library> results,RealmList<Chord_Library> listChordGuitar, String categoryName){
        for(Chord_Library item : results){
            if(item.name.equals(categoryName)){
                listChordGuitar.add(item);
            }
        }
    }

    public static RealmList<Chord_Library> getChordByCategoryName(String categoryName)
    {
        Realm realm = Realm.getDefaultInstance();

        // Query using a predicate string
        RealmQuery query = realm.where(Chord_Library.class);
        if(categoryName.contains("#") == true ||  categoryName.contains("b") == true)
        {
            query.beginsWith("name",categoryName);
            query.equalTo("priority",1);
        }
        else
        {
            query.beginsWith("name",categoryName);
            query.equalTo("priority",1);
            query.not().beginsWith("name",categoryName + "#");
        }


        RealmResults<Chord_Library> results = query.findAll();
        RealmList <Chord_Library> temp = new RealmList<>();
        temp.addAll(results);
        RealmList <Chord_Library> listChordGuitar = new RealmList<>();

        // them hop am B vi search category B khong thay
        if(categoryName.equals("B")){
            listChordGuitar.add(Chord_Library.getChordByChordId("734").first());
        }


        // C
        filterWithCategoryName(temp,listChordGuitar,categoryName);
        // Cm
        filterWithCategoryName(temp,listChordGuitar,categoryName + "m");
        // C7
        filterWithCategoryName(temp,listChordGuitar,categoryName + "7");
        // Cm7
        filterWithCategoryName(temp,listChordGuitar,categoryName + "m7");
        // Cmaj7
        filterWithCategoryName(temp,listChordGuitar,categoryName + "maj7");
        // Cdim
        filterWithCategoryName(temp,listChordGuitar,categoryName + "dim");
        // C5
        filterWithCategoryName(temp,listChordGuitar,categoryName + "5");
        // C6
        filterWithCategoryName(temp,listChordGuitar,categoryName + "6");
        // Cm6
        filterWithCategoryName(temp,listChordGuitar,categoryName + "m6");



        // more
        for(Chord_Library item : temp)
        {
            if (listChordGuitar.contains(item) == false) {
                listChordGuitar.add(item);
            }
        }


        return listChordGuitar;
    }


    public static RealmList<Chord_Library> suggestChordResultSearch(String textSearch)
    {
        Realm realm = Realm.getDefaultInstance();

        // Query using a predicate string
        RealmQuery query = realm.where(Chord_Library.class);
        query.beginsWith("name",textSearch,Case.INSENSITIVE);
        query.equalTo("priority",1);
        RealmResults<Chord_Library> results = query.findAll().sort("name");
        RealmList <Chord_Library> listChordGuitar = new RealmList<>();

        // them hop am B vi search category B khong thay
        if(textSearch.toLowerCase().equals("b")){
            listChordGuitar.add(Chord_Library.getChordByChordId("734").first());
        }

        listChordGuitar.addAll(results);
        return listChordGuitar;
    }


    public static RealmList<Chord_Library> searchChord(String textSearch)
    {
        Realm realm = Realm.getDefaultInstance();

        // Query using a predicate string
        RealmQuery query = realm.where(Chord_Library.class);
        query.equalTo("name",textSearch,Case.INSENSITIVE);
        query.equalTo("priority",1);
        RealmResults<Chord_Library> results = query.findAll().sort("name");
        RealmList <Chord_Library> listChordGuitar = new RealmList<>();

        // them hop am B vi search category B khong thay
        if(textSearch.toLowerCase().equals("b")){
            listChordGuitar.add(Chord_Library.getChordByChordId("734").first());
        }

        listChordGuitar.addAll(results);
        return listChordGuitar;
    }
}
