package com.kabigon.guitarchord.Model;

import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by Kabi on 9/9/16.
 */
public class User extends RealmObject {

    static public String USERNAME_KEY = "username";
    static public String EMAIL_KEY = "email";
    static public String PASSWORD_KEY = "password";

    @PrimaryKey
    @Required
    public String rowId = "";

    @Required
    public String username = "";

    @Required
    public String email = "";

    @Required
    public String password = "";

    static public void insertUser(String username,String email,String password)
    {
        // Create a User object
        User user = new User();
        user.rowId = UUID.randomUUID().toString();
        user.username = username;
        user.email = email;
        user.password = password;

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealm(user);
        realm.commitTransaction();
    }



    static public void updateUser(String username,String email,String password,User user)
    {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        // Update a User object
        user.username = username;
        user.email = email;
        user.password = password;
        realm.copyToRealmOrUpdate(user);
        realm.commitTransaction();
    }



    static public User getUserInfo()
    {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery query = realm.where(User.class);
        RealmResults<User> results = query.findAll();

        if(results.size() != 0){
            return results.first();
        }

        return null;
    }


    static public void deleteUserInfo(User user)
    {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        user.deleteFromRealm();
        realm.commitTransaction();
    }
}
