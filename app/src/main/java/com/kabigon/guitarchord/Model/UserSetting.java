package com.kabigon.guitarchord.Model;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by Kabi on 7/26/16.
 */
public class UserSetting extends RealmObject {

    @PrimaryKey
    @Required
    public String key = "";

    @Required
    public String value = "";


    public static void updateSetting(String key, String value){
        // Obtain a Realm instance
        Realm realm = Realm.getDefaultInstance();
        if(realm.isInTransaction()){
            UserSetting userSetting = new UserSetting();
            userSetting.key = key;
            userSetting.value = value;
            realm.copyToRealmOrUpdate(userSetting);
        }
        else{
            realm.beginTransaction();
            UserSetting userSetting = new UserSetting();
            userSetting.key = key;
            userSetting.value = value;
            realm.copyToRealmOrUpdate(userSetting);
            realm.commitTransaction();
        }
    }

    public static String getValueOfKey(String key){
        Realm realm = Realm.getDefaultInstance();
        RealmQuery query = realm.where(UserSetting.class).equalTo("key", key);
        RealmResults<UserSetting> results = query.findAll();

        if(results.size() > 0)
        {
            return results.first().value;
        }
        return "";
    }

}
