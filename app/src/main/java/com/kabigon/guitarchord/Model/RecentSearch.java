package com.kabigon.guitarchord.Model;

import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import io.realm.annotations.Required;
import com.kabigon.guitarchord.Common.AppSetting;

/**
 * Created by Kabi on 8/10/16.
 */
public class RecentSearch extends RealmObject {


    @Required
    public String keyword = "";

    @Required
    public Date dateSearch = new Date();

    public int countNumberSearchKeyword = 1;

    @Required
    public String songId = "";

    @Required
    public String artistId = "";

    @Required
    public Boolean isSearchChordOrSong = false;

    @Required
    public String isUkuleleChord = "1";


    /********************************************************************************/
    // MARK: -  Update & Insert

    private static void saveResult(RealmResults<RecentSearch> results, Realm realm, String songId,String artistId,Boolean isSearchChordOrSong){
        RecentSearch recent = results.first();
        recent.countNumberSearchKeyword += 1;
        recent.songId = songId;
        recent.artistId = artistId;
        recent.dateSearch = new Date();
        recent.isSearchChordOrSong = isSearchChordOrSong;
        recent.isUkuleleChord = UserSetting.getValueOfKey(AppSetting.kCurrentNhacCu);
        realm.copyToRealm(recent);

    }


    public static void saveKeywordUserSearchToListRecent(String keywordSearching,String songId,String artistId,Boolean isSearchChordOrSong)
    {
        // Get the default Realm
        Realm realm = Realm.getDefaultInstance();

        // kiem tra keyword da duoc search chua co roi thi tang bien dem so lan search
        RealmQuery query = realm.where(RecentSearch.class);
        query.equalTo("keyword",keywordSearching.trim());
        RealmResults<RecentSearch> results = query.findAll();
        if(results.size() != 0)
        {
            if(realm.isInTransaction())
            {
                saveResult(results,realm,songId,artistId,isSearchChordOrSong);
            }
            else
            {
                realm.beginTransaction();
                saveResult(results,realm,songId,artistId,isSearchChordOrSong);
                realm.commitTransaction();
            }

        }
        // add new keyword
        else
        {
            // Create a RecentSearch object
            RecentSearch recent = new RecentSearch();
            recent.keyword = keywordSearching.trim();
            recent.songId = songId;
            recent.artistId = artistId;
            recent.dateSearch = new Date();
            recent.isSearchChordOrSong = isSearchChordOrSong;
            recent.isUkuleleChord = UserSetting.getValueOfKey(AppSetting.kCurrentNhacCu);

            // You only need to do this once (per thread)

            // Add to the Realm inside a transaction
            if(realm.isInTransaction() == true)
            {
                realm.copyToRealm(recent);
            }
            else
            {
                realm.beginTransaction();
                realm.copyToRealm(recent);
                realm.commitTransaction();
            }

        }
    }


    /********************************************************************************/


    /********************************************************************************/
    // MARK: -  Query


    public static RealmList<RecentSearch> getListChordRecentSearch()
    {
        return findListRecentSearch(true);
    }



    public static RealmList<RecentSearch> getListSongRecentSearch()
    {
        return findListRecentSearch(false);
    }


    public static RealmList<RecentSearch> findListRecentSearch(Boolean isSearchChordOrSong)
    {
        // Get the default Realm
        Realm realm = Realm.getDefaultInstance();
        RealmQuery query = realm.where(RecentSearch.class);

        if(isSearchChordOrSong == true)
        {
            query.equalTo("isSearchChordOrSong",isSearchChordOrSong);
            query.equalTo("isUkuleleChord",UserSetting.getValueOfKey(AppSetting.kCurrentNhacCu));
        }
        else
        {
            query.equalTo("isSearchChordOrSong",isSearchChordOrSong);
        }


        RealmResults<RecentSearch> results = query.findAll().sort("dateSearch", Sort.DESCENDING);
        RealmList <RecentSearch> listRecentSearch = new RealmList<>();

        // limit
        if(results.size() > AppSetting.kLimitNumberItemInRecentSearch)
        {

            listRecentSearch.addAll(results.subList(0,AppSetting.kLimitNumberItemInRecentSearch - 1));

            if(results.size() > 2 * AppSetting.kLimitNumberItemInRecentSearch){
                realm.beginTransaction();
                for(int i = AppSetting.kLimitNumberItemInRecentSearch + 1; i < results.size(); i++){
                    RecentSearch recentSearch = results.get(i);
                    recentSearch.deleteFromRealm();
                }
                realm.commitTransaction();
            }


            return listRecentSearch;
        }
        else
        {
            listRecentSearch.addAll(results);
            return listRecentSearch;
        }
    }


    /********************************************************************************/
}
