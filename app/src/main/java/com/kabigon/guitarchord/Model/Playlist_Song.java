package com.kabigon.guitarchord.Model;

import java.util.ArrayList;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.annotations.Required;

/**
 * Created by Kabi on 8/12/16.
 */
public class Playlist_Song extends RealmObject {

    public int playlistId = 0;

    @Required
    public String songId = "";

    @Required
    public Date dateAdd = new Date();

    /********************************************************************************/
    // MARK: - Insert row


    public static Boolean addSongToPlaylist(int playlistId,String songId)
    {
        // Get the default Realm
        Realm realm = Realm.getDefaultInstance();
        // You only need to do this once (per thread)

        // kiem tra bài hát đã nằm trong playlist chưa
        RealmQuery query = realm.where(Playlist_Song.class).equalTo("songId", songId);
        query.equalTo("playlistId", playlistId);
        RealmResults<Playlist_Song> results = query.findAll();

        // add vào nếu chưa
        if (results.size() == 0)
        {
            // Create a Playlist_Song object
            Playlist_Song object = new Playlist_Song();
            object.playlistId = playlistId;
            object.songId = songId;
            object.dateAdd = new Date();

            // Add to the Realm inside a transaction
            realm.beginTransaction();
            realm.copyToRealm(object);
            realm.commitTransaction();

            return true;
        }

        return false;
    }



    public static void addListSongToPlaylist(int playlistId, ArrayList<String> listSongId,Realm realm)
    {
        for(String songId : listSongId)
        {
            // Create a Playlist_Song object
            Playlist_Song object = new Playlist_Song();
            object.playlistId = playlistId;
            object.songId = songId;
            object.dateAdd = new Date();

            realm.copyToRealm(object);
        }
    }


    public static RealmList<Playlist_Song> getAllSongInPlaylist()
    {
        // Get the default Realm
        Realm realm = Realm.getDefaultInstance();
        // You only need to do this once (per thread)

        // kiem tra bài hát đã nằm trong playlist chưa
        RealmQuery query = realm.where(Playlist_Song.class);
        RealmResults<Playlist_Song> results = query.findAll();

        RealmList <Playlist_Song> listPlaylist_Song = new RealmList<>();
        listPlaylist_Song.addAll(results);
        return listPlaylist_Song;
    }


    /********************************************************************************/


    /********************************************************************************/
    // MARK: - Delete


    static public void deleteAllSongInPlaylist(int playlistId,Realm realm)
    {
        // kiem tra bài hát đã nằm trong playlist chưa
        RealmQuery query = realm.where(Playlist_Song.class);
        query.equalTo("playlistId",playlistId);
        RealmResults<Playlist_Song> results = query.findAll();

        results.deleteAllFromRealm();
    }



    static public void removeSongFromPlaylist(int playlistId,String songId)
    {
        // Get the default Realm
        Realm realm = Realm.getDefaultInstance();
        // You only need to do this once (per thread)

        // kiem tra bài hát đã nằm trong playlist chưa
        RealmQuery query = realm.where(Playlist_Song.class);
        query.equalTo("songId",songId);
        query.equalTo("playlistId",playlistId);
        RealmResults<Playlist_Song> results = query.findAll();

        // Delete neu tim thay
        if(results.size() != 0)
        {
            realm.beginTransaction();
            results.first().deleteFromRealm();
            realm.commitTransaction();
        }
    }

    /********************************************************************************/
}
