package com.kabigon.guitarchord.Common;

/**
 * Created by Kabi on 7/26/16.
 */
public class AppSetting {

    public static String kLinkShare = "http://hopamchuan.com/song/";


    public static String kFontSizeLyrics = "FontSizeLyrics";

    // Nhac Cu
    public static String kCurrentNhacCu = "CurrentNhacCu";

    // limit recent search
    public static int kLimitNumberItemInRecentSearch = 40;

    // limit recent view
    public static int kLimitNumberItemInRecentView = 200;


    // sort type : 0 -> by name | 1 -> by date
    public static int sortTypeInPlaylist = 0;

    // DB
    public static String kCurrentVersionDB = "CurrentVersionDB";

    // DateTime DB From HAC
    public static String kDateTimeLastUpdateDBFromHAC = "DateTimeLastUpdateDBFromHAC";

    // auto update song
    public static String kAllowAutoUpdateSong = "AllowAutoUpdateSong";

    // auto sync playlist & favourite
    public static String kAllowAutoSyncPlaylistAndFavourite = "AllowAutoSyncPlaylistAndFavourite";


    // skip login
    public static String kSkipLogin = "SkipLogin";


    public static String inAppPurchaseId = "com.hac.android.guitarchord";


}
