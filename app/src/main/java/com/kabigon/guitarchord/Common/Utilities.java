package com.kabigon.guitarchord.Common;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.widget.ListView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.appupdater.AppUpdater;
import com.github.javiersantos.appupdater.AppUpdaterUtils;
import com.github.javiersantos.appupdater.enums.AppUpdaterError;
import com.github.javiersantos.appupdater.enums.Display;
import com.github.javiersantos.appupdater.enums.UpdateFrom;
import com.github.javiersantos.appupdater.objects.Update;


import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.RealmList;
import com.kabigon.guitarchord.Model.User;
import com.kabigon.guitarchord.Model.UserSetting;
import com.kabigon.guitarchord.R;


public class Utilities {
	
	/**
	 * Function to convert milliseconds time to
	 * Timer Format
	 * Hours:Minutes:Seconds
	 * */
	public String milliSecondsToTimer(long milliseconds){
		String finalTimerString = "";
		String secondsString = "";
		
		// Convert total duration into time
		   int hours = (int)( milliseconds / (1000*60*60));
		   int minutes = (int)(milliseconds % (1000*60*60)) / (1000*60);
		   int seconds = (int) ((milliseconds % (1000*60*60)) % (1000*60) / 1000);
		   // Add hours if there
		   if(hours > 0){
			   finalTimerString = hours + ":";
		   }
		   
		   // Prepending 0 to seconds if it is one digit
		   if(seconds < 10){ 
			   secondsString = "0" + seconds;
		   }else{
			   secondsString = "" + seconds;}
		   
		   finalTimerString = finalTimerString + minutes + ":" + secondsString;
		
		// return timer string
		return finalTimerString;
	}
	
	/**
	 * Function to get Progress percentage
	 * @param currentDuration
	 * @param totalDuration
	 * */
	public int getProgressPercentage(long currentDuration, long totalDuration){
		Double percentage = (double) 0;
		
		long currentSeconds = (int) (currentDuration / 1000);
		long totalSeconds = (int) (totalDuration / 1000);
		
		// calculating percentage
		percentage =(((double)currentSeconds)/totalSeconds)*100;
		
		// return percentage
		return percentage.intValue();
	}

	/**
	 * Function to change progress to timer
	 * @param progress - 
	 * @param totalDuration
	 * returns current duration in milliseconds
	 * */
	public int progressToTimer(int progress, int totalDuration) {
		int currentDuration = 0;
		totalDuration = (int) (totalDuration / 1000);
		currentDuration = (int) ((((double)progress) / 100) * totalDuration);
		
		// return current duration in milliseconds
		return currentDuration * 1000;
	}


	public static boolean isNetworkAvailable(final Context context) {
		final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
		return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
	}

	public static void showAlertMessage(Context context,String title,String message){

		// remove playlist
		MaterialDialog.Builder builder = new MaterialDialog.Builder(context);
		builder.title(title);
		builder.content(message);
		builder.cancelable(true);
		builder.positiveText("OK");
		builder.onPositive(new MaterialDialog.SingleButtonCallback() {
			@Override
			public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
				// TODO
				try {
					if(dialog != null && dialog.isShowing()){
						dialog.dismiss();
					}
				} catch (final IllegalArgumentException e) {
					// Handle or log or ignore
				} catch (final Exception e) {
					// Handle or log or ignore
				} finally {
					dialog = null;
				}


			}
		});



		if (!((Activity) context).isFinishing()) {
			builder.show();
		}


	}


	public static int convertDipToPixels(float dips,Context context)
	{
		return (int) (dips * context.getResources().getDisplayMetrics().density + 0.5f);
	}


	static MaterialDialog loading;
	static Handler handler;
	public static void showLoadingPopup(Context context){
		// TODO
		try {
			if(loading != null && loading.isShowing()){
				loading.dismiss();
			}
		} catch (final IllegalArgumentException e) {
			// Handle or log or ignore
		} catch (final Exception e) {
			// Handle or log or ignore
		} finally {
			loading = null;
		}

		loading = new MaterialDialog.Builder(context)
				.title("Loading")
				.content("Đợi một lát...")
				.progress(true, 0)
				.canceledOnTouchOutside(false)
				.show();

		// do the onItemClick action
		if(handler == null) {
			handler = new Handler();
		}
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				hideLoadingPopup();
			}
		}, 15000);
	}

	public static void hideLoadingPopup(){
		// TODO
		try {
			if(loading != null && loading.isShowing()){
				loading.dismiss();
				if(handler != null){
					handler.removeCallbacksAndMessages(null);
				}
			}
		} catch (final IllegalArgumentException e) {
			// Handle or log or ignore
		} catch (final Exception e) {
			// Handle or log or ignore
		} finally {
			loading = null;
			if(handler != null){
				handler.removeCallbacksAndMessages(null);
			}
		}

	}

	public static String getFirstLyrics(String content){
		String firstLyrics;
		if(content.length() > 150)
		{
			firstLyrics = content.substring(0,148);
		}
		else
		{
			firstLyrics = content;
		}

		return firstLyrics.trim().replace("\n"," ");
	}


	public static String stripUnicode(String keyword){
		String text = keyword.replace("đ", "d");
		text = text.replace("Đ", "D");
		text = Normalizer.normalize(text, Normalizer.Form.NFD);
		String resultString = text.replaceAll("[^\\x00-\\x7F]", "");
		return resultString;
	}




	public static String DateToString(Date date){
		SimpleDateFormat simpleDate =  new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String strDt = simpleDate.format(date);
		return strDt;
	}


	public static Date StringToDate(String dateString){
		SimpleDateFormat simpleDate =  new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		try {
			Date date = simpleDate.parse(dateString);
			return date;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new Date();
	}


	public static float pixelsToSp(Context context, float px) {
		float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
		return px/scaledDensity;
	}


	public static boolean isTablet(Context context) {
		return (context.getResources().getConfiguration().screenLayout
				& Configuration.SCREENLAYOUT_SIZE_MASK)
				>= Configuration.SCREENLAYOUT_SIZE_LARGE;
	}









	static public boolean hasNavBar ()
	{
		boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
		boolean hasHomeKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_HOME);

		return (!(hasBackKey && hasHomeKey));
	}


	static public void setDividerListView(ListView listView,Context context){
		listView.setDivider(new ColorDrawable(0xFFDCDCDC));
		listView.setDividerHeight(1);
	}




	/********************************************************************************/












	//********************************************************************************//
	// Check update app
//	static AppUpdater appUpdater;
//	static FinestWebView webView;
	static AppUpdaterUtils appUpdaterUtils;
	public static void checkUpdateApp(final Context context){


		// check internet
		if(!Utilities.isNetworkAvailable(context))
		{
			return;
		}

		if(appUpdaterUtils == null){
			appUpdaterUtils = new AppUpdaterUtils(context)
					.setUpdateFrom(UpdateFrom.GOOGLE_PLAY)
					.withListener(new AppUpdaterUtils.UpdateListener() {
						@Override
						public void onSuccess(final Update update, Boolean isUpdateAvailable) {
//							Log.d("AppUpdater", update.getLatestVersion() + ", " + update.getUrlToDownload() + ", " + Boolean.toString(isUpdateAvailable));

							if(!isUpdateAvailable){
								return;
							}

							// remove playlist
							MaterialDialog.Builder builder = new MaterialDialog.Builder(context);
							builder.title("New update available!");
							builder.content("Update is available to download.\nDownloading the latest update you will get the latest features, improvements and bug fixes.");
							builder.cancelable(true);
							builder.positiveText("UPDATE");
							builder.negativeText("Remind Me Later");
							builder.onPositive(new MaterialDialog.SingleButtonCallback() {
								@Override
								public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
									// TODO
									if (!((Activity) context).isFinishing()) {
										if(context != null){
											final String appPackageName = "com.kabigon.guitarchord"; // getPackageName() from Context or Activity object
											try {
												context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
											} catch (android.content.ActivityNotFoundException anfe) {
												context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
											}
//											context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.hac.android.guitarchord")));
										}
									}
									dialog.dismiss();
								}
							});

							builder.onNegative(new MaterialDialog.SingleButtonCallback() {
								@Override
								public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
									// TODO
									dialog.dismiss();
								}
							});

							try {
								if (!((Activity) context).isFinishing()) {
									builder.show();
								}
							} catch (final IllegalArgumentException e) {
								// Handle or log or ignore
							} catch (final Exception e) {
								// Handle or log or ignore
							} finally {
							}

						}

						@Override
						public void onFailed(AppUpdaterError error) {
							Log.d("AppUpdater", "Something went wrong");
						}});
		}
		appUpdaterUtils.start();

	}

}
