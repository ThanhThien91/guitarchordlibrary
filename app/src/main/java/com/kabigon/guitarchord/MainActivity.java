package com.kabigon.guitarchord;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.github.fernandodev.easyratingdialog.library.EasyRatingDialog;
import com.google.android.gms.ads.MobileAds;
import com.kabigon.guitarchord.Common.Utilities;
import com.kabigon.guitarchord.Controller.AllChord.AllChordScreen;
import com.kabigon.guitarchord.Controller.SearchView.SearchSongActivity;
import com.kabigon.guitarchord.Model.UserSetting;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;
import io.realm.DynamicRealm;
import io.realm.DynamicRealmObject;
import io.realm.FieldAttribute;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmFieldType;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;

public class MainActivity extends AppCompatActivity {


    // search Song
    Intent searchSong;

    // rate app
    EasyRatingDialog easyRatingDialog;

    AllChordScreen allChordScreen;

    @BindView(R.id.toolbar_title)
    TextView toolbar_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
//        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // init database
        this.initDatabase();

        // Crashlytics
        Fabric.with(this, new Crashlytics());
        // Answers
        Fabric.with(this, new Answers(), new Crashlytics());

        Fabric.with(this, new Crashlytics());

        // rate app
        easyRatingDialog = new EasyRatingDialog(this);

        // Google Ads
        MobileAds.initialize(getApplicationContext(), "ca-app-pub-6600916870563331~9912029805");

        // Google Analytics
        // Obtain the shared Tracker instance.
        try{
            MyApplication application = (MyApplication) getApplication();
            application.mTracker = application.getDefaultTracker();
        }catch(Exception exception){
            exception.printStackTrace();
        }

        allChordScreen = AllChordScreen.newInstance(this);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_main_activity, allChordScreen)
                .commit();




        // Search View
        this.findViewById(R.id.ButtonChangeInstrument).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(allChordScreen != null && allChordScreen instanceof AllChordScreen){
                    allChordScreen.showChangeNhacCuDialog();
                }
            }
        });

        // Search View
        this.findViewById(R.id.ButtonSearchSong).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Whatever
                if(searchSong == null){
                    searchSong = new Intent(getBaseContext(),SearchSongActivity.class);
                }

                searchSong.putExtra(SearchSongActivity.ISSEARCHCHORD_PARAMETER, true);

                startActivity(searchSong);

            }
        });

        toolbar_title.setText("  Guitar Chords Plus");

        // default value
        if(UserSetting.getValueOfKey("ShowDialogSelectInstrument").length() == 0){
            if(allChordScreen != null && allChordScreen instanceof AllChordScreen){
                allChordScreen.showChangeNhacCuDialog();
            }
            UserSetting.updateSetting("ShowDialogSelectInstrument","1");
        }

    }

    @Override
    public void onResume()
    {
        super.onResume();

        MyApplication myApp = (MyApplication)this.getApplication();
        if (myApp.wasInBackground)
        {
            Utilities.checkUpdateApp(this);

            // rate app
            easyRatingDialog.showIfNeeded();
        }

        myApp.wasInBackground = false;
    }

    ////////////////////////////////// Init Database //////////////////////////

    static RealmConfiguration config;
    public void initDatabase(){
        // init database
        String realmPath = new File(this.getFilesDir(), "hac.realm").getAbsolutePath();
        File file = new File(realmPath);
        if(!file.exists()) {
            copyBundledRealmFile(this.getResources().openRawResource(R.raw.hac), "hac.realm");
        }




        if (config == null)
        {
            // Example migration adding a new class
            RealmMigration migration = new RealmMigration() {
                @Override
                public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {

                    // DynamicRealm exposes an editable schema
                    RealmSchema schema = realm.getSchema();

                }
            };

            config = new RealmConfiguration.Builder(this)
                    .name("hac.realm")
                    .migration(migration)
                    .schemaVersion(1)
                    .build();
            Realm.setDefaultConfiguration(config);

            // default value
            if(UserSetting.getValueOfKey("CurrentNhacCu").length() == 0){
                UserSetting.updateSetting("CurrentNhacCu","1");
            }

        }
    }


    private String copyBundledRealmFile(InputStream inputStream, String outFileName) {
        try {
            this.getFilesDir().getAbsolutePath();
            File file = new File(this.getFilesDir(), outFileName);
            FileOutputStream outputStream = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int bytesRead;
            while ((bytesRead = inputStream.read(buf)) > 0) {
                outputStream.write(buf, 0, bytesRead);
            }
            outputStream.close();

            return file.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    //////////////////////////////////////////////////////////////////////////
}
