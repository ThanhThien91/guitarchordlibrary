package kabigon.com.hopamchuan.Controller.Chord;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viewpagerindicator.CirclePageIndicator;

import kabigon.com.hopamchuan.Common.AppSetting;
import kabigon.com.hopamchuan.Common.Utilities;
import kabigon.com.hopamchuan.Model.ChordUkulele;
import kabigon.com.hopamchuan.Model.Chord_Library;
import kabigon.com.hopamchuan.Model.Piano_Chord;
import kabigon.com.hopamchuan.Model.UserSetting;
import kabigon.com.hopamchuan.R;

public class DialogChordView extends Dialog {

    private Context context;
    private String chordName;
    private String instrumentType = "1";

    public DialogChordView(Context context,String chordName){
        super(context,R.style.PopupChordStyle);
        this.context = context;
        this.chordName = chordName;
        this.instrumentType = UserSetting.getValueOfKey(AppSetting.kCurrentNhacCu);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_chord_view);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);

        ChordPagerAdapter adapter;
        // Guitar
        if(instrumentType.equalsIgnoreCase("1")){
            viewPager.setLayoutParams(new LinearLayout.LayoutParams(Utilities.convertDipToPixels(210,context),Utilities.convertDipToPixels(210,context)));
            adapter = new ChordPagerAdapter(Chord_Library.getChordByName(this.chordName), context);
        }
        // Ukulele
        else if(instrumentType.equalsIgnoreCase("2")){
            viewPager.setLayoutParams(new LinearLayout.LayoutParams(Utilities.convertDipToPixels(210,context),Utilities.convertDipToPixels(210,context)));
            adapter = new ChordPagerAdapter(context, ChordUkulele.queryChordWithChordName(this.chordName));
        }
        else{
            viewPager.setLayoutParams(new LinearLayout.LayoutParams(Utilities.convertDipToPixels(270,context),Utilities.convertDipToPixels(150,context)));
            adapter = new ChordPagerAdapter(Piano_Chord.getChordWithName(this.chordName), context,"");
        }

        viewPager.setAdapter(adapter);

        //Bind the title indicator to the adapter
        CirclePageIndicator mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
        mIndicator.setViewPager(viewPager);
        mIndicator.setFillColor(AppSetting.highlightChordColor);
        mIndicator.setPageColor(Color.rgb(221,227,229));


        TextView mTitle = (TextView)findViewById(android.R.id.title);
        if(mTitle != null){
            mTitle.setTextColor(AppSetting.highlightChordColor);
        }
    }
}
