package com.kabigon.guitarchord.Controller.SearchView;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.ads.AdRequest;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.kabigon.guitarchord.Common.AppSetting;
import com.kabigon.guitarchord.Common.Utilities;
import com.kabigon.guitarchord.Controller.Chord.DialogChordView;
import com.kabigon.guitarchord.Library.EditTextWithDeleteButton;
import com.kabigon.guitarchord.Model.ChordUkulele;
import com.kabigon.guitarchord.Model.Chord_Library;
import com.kabigon.guitarchord.Model.RecentSearch;
import com.kabigon.guitarchord.Model.UserSetting;
import com.kabigon.guitarchord.MyApplication;
import com.kabigon.guitarchord.R;


public class SearchSongActivity extends AppCompatActivity {


    @BindView(R.id.TextViewSearch)
    EditTextWithDeleteButton TextViewSearch;

    @BindView(R.id.ButtonCancel)
    Button ButtonCancel;

    @BindView(R.id.listViewSearch)
    ListView listViewSearch;

    @BindView(R.id.mainView)
    RelativeLayout mainView;



    @BindView(R.id.textViewRecentSearch)
    TextView textViewRecentSearch;

    @BindView(R.id.textViewMessage)
    TextView textViewMessage;


    // para
    public final static String ARTISTID_PARAMETER = "kabigon.com.hopamchuan.ARTIST_PARAMETER";
    public final static String ARTISTNAME_PARAMETER = "kabigon.com.hopamchuan.ARTISTNAME_PARAMETER";

    SearchChordAdapter searchChordAdapter;
    List<Object> listSong;
    List<Object> listArtist;
    List<Object> listRecentSearch;
    List<Object> listUkuleleChord;
    List<Object> listGuitarChord;

    Handler handler;

    // search
    Boolean isSearching = false;

    // add playlist
    int currentPlaylistId = 0;

    // search  chord
    boolean isSearchChord = false;

    public final static String SONGID_PARAMETER = "kabigon.com.hopamchuan.SONGID_PARAMETER";
    public final static String ISSEARCHCHORD_PARAMETER = "kabigon.com.hopamchuan.ISSEARCHCHORD_PARAMETER";


    // Ads
    Boolean isShowBannerAds = false;

    public Boolean detectEnglish(String string){
        for (int i = 0; i < string.length(); i++) {
            char character = string.charAt(i);
            if (Character.UnicodeBlock.of(character) != Character.UnicodeBlock.BASIC_LATIN) {
                // replace with Y
                return false;
            }
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_search_song);
        ButterKnife.bind(this);

        // don't turn off screen
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // init
        this.listArtist = new ArrayList<>();
        this.listSong = new ArrayList<>();
        this.listRecentSearch = new ArrayList<>();
        this.listUkuleleChord = new ArrayList<>();
        this.listGuitarChord = new ArrayList<>();

        // hint color
        TextViewSearch.editText.setHintTextColor(Color.argb(35,255,255,255));



        // search chord
        if(getIntent().hasExtra(SearchSongActivity.ISSEARCHCHORD_PARAMETER) == true){
            isSearchChord = getIntent().getBooleanExtra(SearchSongActivity.ISSEARCHCHORD_PARAMETER,false);
        }




        // Recent Search
        showListRecentSearchResult();


        // Back
        ButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Whatever
                finish();
            }
        });


        // select item
        listViewSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,final int position, long id) {
                // do the onItemClick action
                dismissKeyboard();
                if(isSearchChord){
                    showChord(position);
                }

            }
        });

        listViewSearch.setDivider(null);
        listViewSearch.setOnScrollListener(new AbsListView.OnScrollListener(){
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                // TODO Auto-generated method stub
            }
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // TODO Auto-generated method stub


                //  scrolling
                if(scrollState == 1){
                    dismissKeyboard();
                }

            }
        });


        // text change listen
        TextViewSearch.addTextChangedListener(editTextChanged());

        this.showKeyboard();
        TextViewSearch.editText.setOnEditorActionListener(
                new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if(event == null){
                            return false;
                        }

                        if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                                actionId == EditorInfo.IME_ACTION_DONE ||
                                event.getAction() == KeyEvent.ACTION_DOWN &&
                                        event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                            searchSongWithType();
                            dismissKeyboard();
                            return true; // consume.
                        }
                        return false; // pass on to other listeners.
                    }
                });


        // Google Analytics
        if(isSearchChord){
            MyApplication.trackerScreen("Search Chord Screen");
        }
        else{
            MyApplication.trackerScreen("Search Song Screen");
        }

//        loadBannerAds();

    }



    void dismissKeyboard(){

        int heightDiff = mainView.getRootView().getHeight() - mainView.getHeight();
        if (heightDiff > Utilities.convertDipToPixels(200,this)) { // if more than 200 dp, it's probably a keyboard...
            // ... do something here
            TextViewSearch.clearFocus();
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            InputMethodManager im = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(TextViewSearch.getWindowToken(), 0);

            // scroll to top
//            listViewSearch.setSelectionAfterHeaderView();
        }
    }


    void showKeyboard(){
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Write your logic here
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
//        TextViewSearch.editText.requestFocus();
//        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.showSoftInput(TextViewSearch.editText, InputMethodManager.SHOW_IMPLICIT);
        super.onStart();
    }



    @Override
    protected void onResume() {
        super.onResume();

        MyApplication myApp = (MyApplication)this.getApplication();
        if (myApp.wasInBackground)
        {
            Utilities.checkUpdateApp(this);
        }

        myApp.wasInBackground = false;
    }

    private EditTextWithDeleteButton.TextChangedListener editTextChanged() {
        return new EditTextWithDeleteButton.TextChangedListener() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {


                String keyword = TextViewSearch.editText.getText().toString();
                if(keyword.length() == 0){
                    showListRecentSearchResult();
                    textViewRecentSearch.setText("History");
                }
                else{
                    // search chord
                    if(isSearchChord == true){
                        textViewRecentSearch.setText("Results");
                    }
                    else{
                        textViewRecentSearch.setVisibility(View.INVISIBLE);
                    }

                }

                // do the onItemClick action
                if(handler == null) {
                    handler = new Handler();
                }
                else{
                    handler.removeCallbacksAndMessages(null);
                }
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        suggestSongWithType();
                    }
                }, 250);

            }

            @Override
            public void beforeTextChanged(CharSequence s, int arg1, int arg2, int arg3) {

            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        };
    }



    @Override
    public void onDestroy() {
        // post notification to reload top song list

        super.onDestroy();
    }

    ////////////////////////////// Search Song With Type //////////////////////////
    public void suggestSongWithType(){

        String keyword = TextViewSearch.editText.getText().toString();

        Log.d("keyword :",keyword);

        // search chord
        if(this.isSearchChord)
        {
            // Guitar
            if(UserSetting.getValueOfKey(AppSetting.kCurrentNhacCu).equals("1"))
            {
                if(keyword.length() != 0){
                    this.listGuitarChord = new ArrayList<>();
                    this.listGuitarChord.addAll(Chord_Library.suggestChordResultSearch(keyword));
                    reloadListGuitarChordResult();
                }
            }
            else
            {
                if(keyword.length() != 0){
                    this.listUkuleleChord = new ArrayList<>();
                    this.listUkuleleChord.addAll(ChordUkulele.suggestChordResultSearch(keyword));
                    reloadListUkuleleChordResult();
                }
            }
        }
    }


    public void searchSongWithType(){

        // search chord
        if(isSearchChord == true){
            textViewRecentSearch.setText("Results");
        }
        else{
            textViewRecentSearch.setVisibility(View.INVISIBLE);
        }


        if(handler != null) {
            handler.removeCallbacksAndMessages(null);
        }

        String keyword = TextViewSearch.editText.getText().toString().trim();

        if (keyword.length() != 0)
        {
            // Search Chord
            if(isSearchChord){
                // Guitar
                if(UserSetting.getValueOfKey(AppSetting.kCurrentNhacCu).equals("1"))
                {
                    this.listGuitarChord = new ArrayList<>();
                    this.listGuitarChord.addAll(Chord_Library.searchChord(keyword));
                    reloadListGuitarChordResult();

                    // co gia tri thi moi luu lai lich su
                    if(this.listGuitarChord.size() != 0)
                    {
                        this.saveKeywordToRecentList(this.listGuitarChord.size(),keyword);
                    }
                }
                else
                {
                    this.listUkuleleChord = new ArrayList<>();
                    this.listUkuleleChord.addAll(ChordUkulele.searchChord(keyword));
                    reloadListUkuleleChordResult();

                    if(this.listUkuleleChord.size() != 0)
                    {
                        this.saveKeywordToRecentList(this.listUkuleleChord.size(),keyword);
                    }
                }
            }
        }
    }











    public void showListRecentSearchResult(){
        textViewRecentSearch.setVisibility(View.VISIBLE);
        if(isSearchChord){
            TextViewSearch.editText.setHint("Search Chord...");
            this.listRecentSearch = new ArrayList<>();
            this.listRecentSearch.addAll(RecentSearch.getListChordRecentSearch());
            reloadListRecentChordResult();
        }


    }


    private void saveKeywordToRecentList(int count,String keyword)
    {
        if(count == 0)
        {
            return;
        }

        // save keyword to list recent
        RecentSearch.saveKeywordUserSearchToListRecent(keyword, "" , "" , false);
    }
















    /********************************************************************************/
    // MARK: - Search Chord

    void showMessageWhenNotFountResult(List<Object> listResult){
        if(listResult.size() == 0){
            listViewSearch.setVisibility(View.INVISIBLE);


            if(isSearchChord == true){
                textViewMessage.setText("Not found chord!");
            }


            // recent search
            if(TextViewSearch.editText.getText().length() == 0){
                textViewMessage.setText("");
            }
            textViewMessage.setVisibility(View.VISIBLE);
        }
        else{
            listViewSearch.setVisibility(View.VISIBLE);
            textViewMessage.setVisibility(View.INVISIBLE);
        }
    }

    public void reloadListGuitarChordResult(){

        // message
        this.showMessageWhenNotFountResult(listGuitarChord);

        int typeSearch = 1;
        if(searchChordAdapter == null){
            searchChordAdapter = new SearchChordAdapter(getBaseContext(), listGuitarChord, listUkuleleChord, listRecentSearch , typeSearch ,listViewSearch);
            listViewSearch.setAdapter(searchChordAdapter);
        }
        else{
            searchChordAdapter.typeData = typeSearch;
            // fire the event
            searchChordAdapter.getListGuitarChordData().clear();
            searchChordAdapter.getListGuitarChordData().addAll(listGuitarChord);
            // fire the event
            searchChordAdapter.notifyDataSetChanged();
        }
    }


    public void reloadListUkuleleChordResult(){

        // message
        this.showMessageWhenNotFountResult(listUkuleleChord);

        int typeSearch = 2;
        if(searchChordAdapter == null){
            searchChordAdapter = new SearchChordAdapter(getBaseContext(), listGuitarChord, listUkuleleChord, listRecentSearch , typeSearch ,listViewSearch);
            listViewSearch.setAdapter(searchChordAdapter);
        }
        else{
            searchChordAdapter.typeData = typeSearch;
            // fire the event
            searchChordAdapter.getListUkuleleChordData().clear();
            searchChordAdapter.getListUkuleleChordData().addAll(listUkuleleChord);
            // fire the event
            searchChordAdapter.notifyDataSetChanged();
        }
    }


    public void reloadListRecentChordResult(){

        // message
        this.showMessageWhenNotFountResult(listRecentSearch);

        if(searchChordAdapter == null){
            searchChordAdapter = new SearchChordAdapter(getBaseContext(), listGuitarChord, listUkuleleChord, listRecentSearch, 3 ,listViewSearch);
            listViewSearch.setAdapter(searchChordAdapter);
        }
        else{
            searchChordAdapter.typeData = 3;
            // fire the event
            searchChordAdapter.getListRecentSearchData().clear();
            searchChordAdapter.getListRecentSearchData().addAll(listRecentSearch);
            // fire the event
            searchChordAdapter.notifyDataSetChanged();
        }
    }

    /********************************************************************************/


    /********************************************************************************/
    // MARK: - Show Chord

    void showChord(int position){
        String chordName = "";
        if(textViewRecentSearch.getText().equals("Results"))
        {
            // Guitar
            if(UserSetting.getValueOfKey(AppSetting.kCurrentNhacCu).equals("1")){
                if(position < this.listGuitarChord.size())
                {
                    Object object = listGuitarChord.get(position);
                    if(object instanceof Chord_Library){
                        chordName = ((Chord_Library) object).name;
                    }

                }
            }
            // Ukulele
            else
            {
                if(position < this.listUkuleleChord.size())
                {
                    Object object = listUkuleleChord.get(position);
                    if(object instanceof ChordUkulele){
                        chordName = ((ChordUkulele) object).name;
                    }
                }
            }
        }
        else
        {
            if(position < this.listRecentSearch.size())
            {
                Object object = listRecentSearch.get(position);
                if(object instanceof RecentSearch){
                    chordName = ((RecentSearch) object).keyword;
                }
            }
        }

        dismissKeyboard();

        if(chordName.length() != 0)
        {
            DialogChordView chordView = new DialogChordView(SearchSongActivity.this,chordName);
            chordView.setCanceledOnTouchOutside(true);
            chordView.setTitle(chordName);
            chordView.show();

            // save chord to list recent
            RecentSearch.saveKeywordUserSearchToListRecent(chordName,"","",true);
        }


    }

    /********************************************************************************/






    void loadBannerAds(){

        final com.google.android.gms.ads.AdView mAdView = (com.google.android.gms.ads.AdView) mainView.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();

        mAdView.setAdListener(new com.google.android.gms.ads.AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                // The ad has been loaded.
                int heightAdView = mAdView.getHeight();

                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
                lp.topMargin = Utilities.convertDipToPixels(10,getBaseContext());
                lp.leftMargin = Utilities.convertDipToPixels(0,getBaseContext());
                lp.bottomMargin = Utilities.convertDipToPixels(10,getBaseContext()) + heightAdView;
                lp.rightMargin = Utilities.convertDipToPixels(10,getBaseContext());
                lp.addRule(RelativeLayout.BELOW, R.id.textViewRecentSearch);
                listViewSearch.setLayoutParams(lp);

                isShowBannerAds = true;
            }
        });
        mAdView.loadAd(adRequest);
    }

    /********************************************************************************/


}
