package com.kabigon.guitarchord.Controller.SearchView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;


import java.util.List;

import com.kabigon.guitarchord.Model.ChordUkulele;
import com.kabigon.guitarchord.Model.Chord_Library;
import com.kabigon.guitarchord.Model.RecentSearch;
import com.kabigon.guitarchord.R;

/**
 * Created by Kabi on 6/28/16.
 */
public class SearchChordAdapter extends BaseAdapter {


    public List<Object> listGuitarChord;
    public List<Object> listUkuleleChord;
    public List<Object> listRecentSearch;
    public int typeData = 1; // 1 -> Guitar | 2 -> Ukulele | 3 -> Recent Search

    Context context;
    ListView listViewSong;

    private static LayoutInflater inflater=null;
    public SearchChordAdapter(Context context, List<Object> listGuitarChord, List<Object> listUkuleleChord,List<Object> listRecentSearch,  int typeData, ListView listViewSong) {
        // TODO Auto-generated constructor stub
        this.listGuitarChord = listGuitarChord;
        this.listUkuleleChord = listUkuleleChord;
        this.listRecentSearch = listRecentSearch;
        this.typeData = typeData;

        this.context = context;
        this.listViewSong = listViewSong;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    public List<Object> getListGuitarChordData() {
        return this.listGuitarChord;
    }
    public List<Object> getListUkuleleChordData() {
        return this.listUkuleleChord;
    }
    public List<Object> getListRecentSearchData() {
        return this.listRecentSearch;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        if(typeData == 1){
            return this.listGuitarChord.size();
        }
        else if(typeData == 2){
            return this.listUkuleleChord.size();
        }
        else{
            return listRecentSearch.size();
        }
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        // Define a way to determine which layout to use, here it's just evens and odds.
        // guitar
        return 1;
    }

    @Override
    public int getViewTypeCount() {
        return 2; // Count of different layouts
    }

    static class ViewHolder
    {
        TextView resultSearch;
        ImageButton icon_row_search;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        // TODO Auto-generated method stub
        // element
        final ViewHolder holder;
        if(convertView == null){
            convertView = inflater.inflate(R.layout.search_row, null);
            holder = new ViewHolder();
            holder.resultSearch = (TextView) convertView.findViewById(R.id.TextViewResultSearch);
            holder.icon_row_search = (ImageButton) convertView.findViewById(R.id.icon_row_search);
            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder) convertView.getTag();
        }

        // guitar
        if(typeData == 1){

            // icon search
            holder.icon_row_search.setImageResource(R.drawable.icon_search);

            if(position < this.listGuitarChord.size())
            {
                final Chord_Library guitarChord = (Chord_Library) this.listGuitarChord.get(position);
                if (guitarChord.rowId.length() == 0) {
                    convertView.setVisibility(View.INVISIBLE);
                } else {
                    convertView.setVisibility(View.VISIBLE);
                }


                if (guitarChord != null) {
                    holder.resultSearch.setText(guitarChord.name);
                }
            }
            else {
                holder.resultSearch.setText("");
                convertView.setVisibility(View.INVISIBLE);
            }
        }
        // ukulele
        else if(typeData == 2){
            if(position < this.listUkuleleChord.size())
            {
                ChordUkulele chordUkulele = (ChordUkulele) this.listUkuleleChord.get(position);
                if(chordUkulele.chord_id.length() == 0)
                {
                    convertView.setVisibility(View.INVISIBLE);
                }
                else
                {
                    convertView.setVisibility(View.VISIBLE);
                }

                holder.resultSearch.setText(chordUkulele.name);
            }
            else
            {
                holder.resultSearch.setText("");
                convertView.setVisibility(View.INVISIBLE);
            }
        }
        // recent search
        else
        {
            // recent search
            holder.icon_row_search.setImageResource(R.drawable.icon_recent_search);

            if(position < this.listRecentSearch.size()) {
                RecentSearch recent = (RecentSearch) this.listRecentSearch.get(position);
                holder.resultSearch.setText(recent.keyword);
            }
        }

        return convertView;

    }



}



