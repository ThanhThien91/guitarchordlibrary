package com.kabigon.guitarchord.Controller.AllChord;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import com.kabigon.guitarchord.R;

/**
 * Created by Kabi on 7/14/16.
 */
public class ChordTabAdapter extends BaseAdapter {


    ArrayList<String> categoryChord;
    Context context;
    private int selectedIndex;
    private static LayoutInflater inflater=null;

    public ChordTabAdapter(Context mainActivity, ArrayList<String> categoryChord) {
        // TODO Auto-generated constructor stub
        this.categoryChord = categoryChord;
        context = mainActivity;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }




    public ArrayList<String> getData() {
        return categoryChord;
    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return categoryChord.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public void setSelectedIndex(int ind)
    {
        selectedIndex = ind;
        notifyDataSetChanged();
    }

    public class Holder
    {
        TextView songTitle;
        View highlightSelect;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        // TODO Auto-generated method stub
        // element
        final Holder holder = new Holder();
        if(convertView == null){
            convertView = inflater.inflate(R.layout.title_chord_tab, null);
        }

        holder.highlightSelect = convertView.findViewById(R.id.highlightSelect);
        if (position == selectedIndex)
        {
            holder.highlightSelect.setVisibility(View.VISIBLE);
        }
        else{
            holder.highlightSelect.setVisibility(View.INVISIBLE);
        }

        String text = categoryChord.get(position);
        holder.songTitle = (TextView) convertView.findViewById(R.id.textViewTabTitle);
        holder.songTitle.setText(text);

        return convertView;
    }
}