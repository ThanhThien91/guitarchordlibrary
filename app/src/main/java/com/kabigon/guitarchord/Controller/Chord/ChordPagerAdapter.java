package kabigon.com.hopamchuan.Controller.Chord;

/**
 * Created by Kabi on 7/1/16.
 */
import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import io.realm.RealmList;
import kabigon.com.hopamchuan.Model.ChordUkulele;
import kabigon.com.hopamchuan.Model.Chord_Library;
import kabigon.com.hopamchuan.Model.Piano_Chord;

public class ChordPagerAdapter extends PagerAdapter {

    private Context mContext;
    private RealmList<Chord_Library> chordGuitarList;
    private RealmList<ChordUkulele> chordUkuleleList;
    private RealmList<Piano_Chord> chordPianoList;
    private String instrumentType = "1";
    ChordDrawView drawView;
    PianoDrawView pianoDrawView;

    public ChordPagerAdapter(RealmList<Chord_Library> chordGuitarList , Context context) {
        mContext = context;
        this.chordGuitarList = chordGuitarList;
        this.instrumentType = "1";
    }

    public ChordPagerAdapter(Context context,RealmList<ChordUkulele> chordUkuleleList) {
        mContext = context;
        this.chordUkuleleList = chordUkuleleList;
        this.instrumentType = "2";
    }

    public ChordPagerAdapter(RealmList<Piano_Chord> chordPianoList , Context context, String piano) {
        mContext = context;
        this.chordPianoList = chordPianoList;
        this.instrumentType = "3";
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {

        // Piano
        if(this.instrumentType.equalsIgnoreCase("3")) {
            Piano_Chord chordObject = chordPianoList.get(position);
            pianoDrawView = new PianoDrawView(mContext, chordObject);

//            pianoDrawView.setBackgroundColor(Color.WHITE);

            // Creating a new RelativeLayout
            RelativeLayout relativeLayout = new RelativeLayout(mContext);

            // Defining the RelativeLayout layout parameters.
            // In this case I want to fill its parent
            RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT);

            relativeLayout.setLayoutParams(rlp);

            PianoDrawView.setBackgroundKeyboard(mContext,relativeLayout,chordObject);
            relativeLayout.addView(pianoDrawView);

            collection.addView(relativeLayout);
            return relativeLayout;
        }
        else{
            // Guitar
            if(this.instrumentType.equalsIgnoreCase("1")) {
                Chord_Library chordObject = chordGuitarList.get(position);
                drawView = new ChordDrawView(mContext, chordObject,10);
            }
            // Ukulele
            else if(this.instrumentType.equalsIgnoreCase("2")) {
                ChordUkulele chordObject = chordUkuleleList.get(position);
                drawView = new ChordDrawView(mContext, chordObject,10);
            }


            drawView.setBackgroundColor(Color.WHITE);

            collection.addView(drawView);
            return drawView;
        }
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        // Guitar
        if(this.instrumentType.equalsIgnoreCase("1")){
            return chordGuitarList.size();
        }
        // Ukulele
        else if(this.instrumentType.equalsIgnoreCase("2")) {
            return chordUkuleleList.size();
        }
        // Guitar
        else{
            return chordPianoList.size();
        }

    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Thien";
    }

}