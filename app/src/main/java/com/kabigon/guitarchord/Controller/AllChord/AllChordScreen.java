package com.kabigon.guitarchord.Controller.AllChord;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.GridView;


import com.google.android.gms.ads.AdRequest;

import java.util.ArrayList;
import java.util.Arrays;

import io.realm.RealmList;
import it.sephiroth.android.library.widget.AdapterView;
import it.sephiroth.android.library.widget.HListView;
import com.kabigon.guitarchord.Common.AppSetting;
import com.kabigon.guitarchord.Common.Utilities;
import com.kabigon.guitarchord.Controller.Chord.DialogChordView;
import com.kabigon.guitarchord.Controller.AllChord.ChangeNhacCuDialog;
import com.kabigon.guitarchord.MainActivity;
import com.kabigon.guitarchord.Model.ChordUkulele;
import com.kabigon.guitarchord.Model.Chord_Library;
import com.kabigon.guitarchord.Model.UserSetting;
import com.kabigon.guitarchord.MyApplication;
import com.kabigon.guitarchord.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AllChordScreen.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AllChordScreen#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AllChordScreen extends Fragment implements ChangeNhacCuDialog.ChangeNhacCuEventListener {
    // TODO: Rename parameter arguments, choose names that match


    // Var
    GridView gridViewChord;

    private static View mainView = null;
    static MainActivity mainActivity;

    private ChordTabAdapter adapter;

    ChangeNhacCuDialog changeNhacCuDialog;


    HListView hListView;
    AllChordAdapter allChordAdapter;
    RealmList<Chord_Library> listChordGuitar;
    RealmList<ChordUkulele> listChordUkulele;

    // Ads
    Boolean isShowBannerAds = false;

    public AllChordScreen() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AllChordView.
     */
    // TODO: Rename and change types and number of parameters
    public static AllChordScreen newInstance(MainActivity mainActivityPass) {
        AllChordScreen fragment = new AllChordScreen();
        mainActivity = mainActivityPass;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainView = inflater.inflate(R.layout.fragment_all_chord_view, container, false);
        gridViewChord = (GridView) mainView.findViewById(R.id.gridViewChord);
        loadData();
        return mainView;
    }


    void loadData()
    {
        final ArrayList<String> categoryChord = new ArrayList(Arrays.asList("  C  ","  D  ","  E  ","  F  ","  G  ","  A  ","  B  ","C#/Db","D#/Eb","F#/Gb","G#/Ab","A#/Bb"));
        hListView = (HListView) mainView.findViewById(R.id.hListViewCategoryChord);
        final ChordTabAdapter chordTabAdapter = new ChordTabAdapter(mainActivity,categoryChord);
        hListView.setAdapter(chordTabAdapter);

        hListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                chordTabAdapter.setSelectedIndex(position);
                createGridView(categoryChord.get(position));
            }
        });

        // default
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                chordTabAdapter.setSelectedIndex(0);
                createGridView(categoryChord.get(0));

                loadBannerAds();
            }
        }, 100);

//        chordTabAdapter.setSelectedIndex(0);
//        createGridView(categoryChord.get(0));

        // Google Analytics
        MyApplication.trackerScreen("All Chord Screen");

    }


    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    ////////////////////////////////// Show Chord //////////////////////////
    void createGridView(String category){

        // guitar
        if(UserSetting.getValueOfKey(AppSetting.kCurrentNhacCu).equals("1")){
            listChordGuitar = this.getChordByCategoryName(category.trim());
            // fill data
            if(listChordGuitar.size() != 0) {
                allChordAdapter = new AllChordAdapter(mainActivity, listChordGuitar,listChordUkulele);
                gridViewChord.setAdapter(allChordAdapter);
            }
        }
        // ukulele
        else{
            listChordUkulele = ChordUkulele.getChordByCategoryName(category.trim());
            // fill data
            if(listChordUkulele.size() != 0) {
                allChordAdapter = new AllChordAdapter(mainActivity, listChordGuitar, listChordUkulele);
                gridViewChord.setAdapter(allChordAdapter);
            }
        }



        gridViewChord.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
            public void onItemClick(android.widget.AdapterView<?> parent, View v,
                                    int position, long id) {
                showChordDialog(position);
            }
        });

    }

    DialogChordView chordView;
    void showChordDialog(int position){

        if (chordView != null && chordView.isShowing()) {
            return;
        }

        // guitar
        if(UserSetting.getValueOfKey(AppSetting.kCurrentNhacCu).equals("1")){
            if(position < listChordGuitar.size()){
                Chord_Library chordObject = listChordGuitar.get(position);
                if(chordObject != null){
                    chordView = new DialogChordView(mainActivity,chordObject.name);
                    chordView.setCanceledOnTouchOutside(true);
                    chordView.setTitle(chordObject.name);
                    chordView.show();

                }
            }
        }
        // ukulele
        else{
            if(position < listChordUkulele.size()){
                ChordUkulele chordObject = listChordUkulele.get(position);
                if(chordObject != null){
                    chordView = new DialogChordView(mainActivity,chordObject.name);
                    chordView.setCanceledOnTouchOutside(true);
                    chordView.setTitle(chordObject.name);
                    chordView.show();
                }
            }
        }

    }

    RealmList<Chord_Library> getChordByCategoryName(String chordNamePass)
    {
        // get all chord in category
        String chordName;

        // hop am giang va thang
        String[] array = chordNamePass.split("/");
        if(array.length != 0)
        {
            chordName = array[0];
        }
        // hop am binh thuong
        else
        {
            chordName = chordNamePass;
        }

        return Chord_Library.getChordByCategoryName(chordName);
    }

    ////////////////////////////////// Change Nhac Cu //////////////////////////
    public void showChangeNhacCuDialog(){
        if(changeNhacCuDialog == null){
            changeNhacCuDialog = new ChangeNhacCuDialog(mainActivity);
        }

        changeNhacCuDialog.setCanceledOnTouchOutside(true);
        changeNhacCuDialog.setTitle("Instrument");
        changeNhacCuDialog.setTheListener(this);
        changeNhacCuDialog.show();
    }

    @Override
    public void closeChangeNhacCuDialog() {
        loadData();
        changeNhacCuDialog.dismiss();
    }


    void loadBannerAds(){

        final com.google.android.gms.ads.AdView mAdView = (com.google.android.gms.ads.AdView) mainView.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.setAdListener(new com.google.android.gms.ads.AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                int heightAdView = mAdView.getHeight();
                FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
                lp.setMargins(Utilities.convertDipToPixels(0,mainActivity),Utilities.convertDipToPixels(44,mainActivity),Utilities.convertDipToPixels(0,mainActivity),heightAdView);
                gridViewChord.setLayoutParams(lp);

                isShowBannerAds = true;
            }
        });
        mAdView.loadAd(adRequest);
    }
}
