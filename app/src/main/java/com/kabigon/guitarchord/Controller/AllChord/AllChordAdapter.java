package com.kabigon.guitarchord.Controller.AllChord;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import io.realm.RealmList;
import com.kabigon.guitarchord.Common.AppSetting;
import com.kabigon.guitarchord.Common.Utilities;
import com.kabigon.guitarchord.Controller.Chord.ChordDrawView;
import com.kabigon.guitarchord.Model.ChordUkulele;
import com.kabigon.guitarchord.Model.Chord_Library;
import com.kabigon.guitarchord.Model.UserSetting;
import com.kabigon.guitarchord.R;


public class AllChordAdapter extends BaseAdapter {
	private Context context;
	private final RealmList<Chord_Library> listChordGuitar;
	private final RealmList<ChordUkulele> listChordUkulele;
	private static LayoutInflater inflater=null;

	int widthChord;
	int heightChord;

	public AllChordAdapter(Context context, RealmList<Chord_Library> listChordGuitar, RealmList<ChordUkulele> listChordUkulele) {
		this.context = context;
		this.listChordGuitar = listChordGuitar;
		this.listChordUkulele = listChordUkulele;
		inflater = ( LayoutInflater )context.
				getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		widthChord = Utilities.convertDipToPixels(100,context);
		heightChord = Utilities.convertDipToPixels(115,context);
	}



	static class ViewHolder
	{
		TextView chordName;
		RelativeLayout contentView;
	}


	public View getView(int position, View convertView, ViewGroup parent) {

		final ViewHolder holder;
		if (convertView == null) {
			// get layout from mobile.xml
			convertView = inflater.inflate(R.layout.chord_cell, parent, false);
			holder = new ViewHolder();
			// set value into textview
			holder.chordName = (TextView) convertView
					.findViewById(R.id.textViewChordName);
			holder.contentView = (RelativeLayout) convertView
					.findViewById(R.id.contentView);


			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		// Guitar
		if(UserSetting.getValueOfKey(AppSetting.kCurrentNhacCu).equals("1")) {
			Chord_Library chordObject = listChordGuitar.get(position);
			if(chordObject != null){
				holder.chordName.setText(chordObject.name);
				ChordDrawView drawView = new ChordDrawView(context, chordObject,7);

				drawView.setLayoutParams(new LinearLayout.LayoutParams(widthChord
						,heightChord));
				holder.contentView.removeAllViews();
				holder.contentView.addView(drawView);
			}
		}
		// Ukulele
		else{
			ChordUkulele chordObject = listChordUkulele.get(position);
			if(chordObject != null){
				holder.chordName.setText(chordObject.name);
				ChordDrawView drawView = new ChordDrawView(context, chordObject,7);
				drawView.setLayoutParams(new LinearLayout.LayoutParams(widthChord
						,heightChord));
				holder.contentView.removeAllViews();
				holder.contentView.addView(drawView);
			}
		}


		return convertView;
	}

	@Override
	public int getCount() {
		// Guitar
		if(UserSetting.getValueOfKey(AppSetting.kCurrentNhacCu).equals("1")) {
			return listChordGuitar.size();
		}
		// Ukulele
		return listChordUkulele.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

}
