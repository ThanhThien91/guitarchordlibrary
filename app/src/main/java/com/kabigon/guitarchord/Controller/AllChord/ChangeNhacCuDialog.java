package com.kabigon.guitarchord.Controller.AllChord;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.kabigon.guitarchord.Common.AppSetting;
import com.kabigon.guitarchord.Common.Utilities;
import com.kabigon.guitarchord.Model.UserSetting;
import com.kabigon.guitarchord.R;

public class ChangeNhacCuDialog extends Dialog {

    ChangeNhacCuEventListener eventListener;
    public interface ChangeNhacCuEventListener {
        // you can define any parameter as per your requirement
        void closeChangeNhacCuDialog();
    }
    public void setTheListener(ChangeNhacCuEventListener listen) {
        eventListener = listen;
    }

    public ChangeNhacCuDialog( ){
        super(null);
    }

    @BindView(R.id.ButtonSelectGuitar)
    Button ButtonSelectGuitar;

    @BindView(R.id.ButtonSelectUkulele)
    Button ButtonSelectUkulele;

    @BindView(R.id.ButtonCancel)
    Button ButtonCancel;

    @BindView(R.id.ButtonOk)
    Button ButtonOk;

    Context context;
    int widthImage;
    int heightImage;

    // Nhac Cụ
    String isSelectGuitar = "1";

    public ChangeNhacCuDialog(Context context){
        super(context,R.style.PopupStyle);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_nhac_cu_dialog);
        ButterKnife.bind(this);

        widthImage = Utilities.convertDipToPixels(20,context);
        heightImage = Utilities.convertDipToPixels(17,context);

        // current nhac cu
        this.isSelectGuitar = UserSetting.getValueOfKey(AppSetting.kCurrentNhacCu);
        if(this.isSelectGuitar.equals("1")){
            setLeftImageOfButton(R.drawable.icon_checkmark,ButtonSelectGuitar);
            setLeftImageOfButton(R.drawable.icon_checkmark_white,ButtonSelectUkulele);
        }
        else{
            setLeftImageOfButton(R.drawable.icon_checkmark,ButtonSelectUkulele);
            setLeftImageOfButton(R.drawable.icon_checkmark_white,ButtonSelectGuitar);
        }

        // Event
        ButtonSelectGuitar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Whatever
                setLeftImageOfButton(R.drawable.icon_checkmark,ButtonSelectGuitar);
                setLeftImageOfButton(R.drawable.icon_checkmark_white,ButtonSelectUkulele);

                isSelectGuitar = "1";
            }
        });

        // Event
        ButtonSelectUkulele.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Whatever
                setLeftImageOfButton(R.drawable.icon_checkmark,ButtonSelectUkulele);
                setLeftImageOfButton(R.drawable.icon_checkmark_white,ButtonSelectGuitar);

                isSelectGuitar = "0";
            }
        });


        // Event
        ButtonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Whatever
                // default value
                UserSetting.updateSetting(AppSetting.kCurrentNhacCu,isSelectGuitar);
                eventListener.closeChangeNhacCuDialog();
            }
        });

        // Event
        ButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Whatever
                eventListener.closeChangeNhacCuDialog();
            }
        });

    }


    private void setLeftImageOfButton(int imageId,Button button){
        Drawable img = getContext().getResources().getDrawable( imageId );
        img.setBounds( 0, 0, widthImage, heightImage );
        button.setCompoundDrawables( img, null, null, null );
    }
}
