package kabigon.com.hopamchuan.Controller.Chord;

/**
 * Created by Kabi on 7/2/16.
 */
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.view.View;

import java.util.ArrayList;

import kabigon.com.hopamchuan.Model.ChordUkulele;
import kabigon.com.hopamchuan.Model.Chord_Library;

public class ChordDrawView extends View {

    Paint paint = new Paint();
    Canvas canvas;

    Boolean isShowUkuleleChord = false;
    Chord_Library chordGuitar;
    ChordUkulele chordUkulele;
    ArrayList<Integer> scheme = new ArrayList();
    String[] fingers;
    Integer viTriNgan = 0;

    int marginTextFr = 10;


    // khoi tao
    Float fontSize = 20.0f;
    Float sizeSoThuTuNgan = 40.0f;
    Float sizeOfPoint = 16.0f;
    Float marginY = 20.0f;
    Integer soNgan = 5;
    Float lineWidthNganDauTien = 5.0f;
    Float lineWidthNganCacNganKhac = 1.5f;
    Float marginX = 15.0f;
    Float widthView = 190.0f;
    Float heightView = 210.0f;



    public ChordDrawView(Context context,Chord_Library chordGuitar,int marginTextFr) {
        super(context);
        paint.setColor(Color.BLACK);
        this.chordGuitar = chordGuitar;
        this.marginTextFr = marginTextFr;

        if (this.chordGuitar != null) {
            this.isShowUkuleleChord = false;

            // scheme
            this.scheme = this.analyticScheme();

            // scheme
            viTriNgan = this.timViTriNgan(scheme);

            // fingers
            this.fingers = this.chordGuitar.fingers.trim().split(" ");
        }
    }

    public void redraw(Chord_Library chordGuitar,int marginTextFr){
        paint.setColor(Color.BLACK);
        this.chordGuitar = chordGuitar;
        this.marginTextFr = marginTextFr;

        if (this.chordGuitar != null) {
            this.isShowUkuleleChord = false;

            // scheme
            this.scheme = this.analyticScheme();

            // scheme
            viTriNgan = this.timViTriNgan(scheme);

            // fingers
            this.fingers = this.chordGuitar.fingers.trim().split(" ");
        }
    }


    public ChordDrawView(Context context,ChordUkulele chordUkulele,int marginTextFr) {
        super(context);
        paint.setColor(Color.BLACK);
        this.chordUkulele = chordUkulele;
        this.marginTextFr = marginTextFr;

        if (this.chordUkulele != null) {
            this.isShowUkuleleChord = true;

            // scheme
            this.scheme = this.analyticSchemeUkulele();

            // scheme
            viTriNgan = this.timViTriNgan(scheme);

            // fingers
            this.fingers = this.chordUkulele.fingers.trim().split(" ");
        }
    }

    @Override
    public void onDraw(Canvas canvas) {
        this.canvas = canvas;

        // width view
        Integer widthDraw = this.getWidth();


        // khoi tao (190 là chiều rộng ở tỉ lệ nhỏ nhất)
        Integer minWidth = 210;
        fontSize = widthDraw / (minWidth / 14.0f) + 1;
        sizeOfPoint = widthDraw / (minWidth / 10.0f);
        sizeSoThuTuNgan = widthDraw / (minWidth / 34.0f);
        marginY =  widthDraw / (minWidth / 20.0f);
        soNgan = 5;
        lineWidthNganDauTien = widthDraw / (minWidth / 5.0f);
        lineWidthNganCacNganKhac = widthDraw / (minWidth / 1.5f);
        marginX = sizeOfPoint/2 + (widthDraw / (minWidth / 10));
        widthView = this.getWidth() - sizeOfPoint - sizeSoThuTuNgan - (widthDraw / (minWidth / 20));
        heightView = this.getHeight() - marginY;


        this.drawRect();
    }


    private void drawRect()
    {
        if (this.chordGuitar == null && this.isShowUkuleleChord == false)
        {
            return;
        }


        if (this.chordUkulele == null && this.isShowUkuleleChord == true)
        {
            return;
        }


        // draw Chord
        // Ngăn đầu tiên tô đậm
        if (viTriNgan == 0)
        {
            PointF fromPoint = new PointF(marginX - 0.5f,lineWidthNganDauTien/2 + marginY);
            PointF startPoint = new PointF(widthView + marginX + 0.5f, lineWidthNganDauTien/2 + marginY);
            this.drawLineCanvas(fromPoint,startPoint,lineWidthNganDauTien);
        }
        else
        {
            PointF fromPoint = new PointF(marginX - 0.5f, marginY);
            PointF startPoint = new PointF(widthView + marginX + 0.5f, marginY);
            this.drawLineCanvas(fromPoint,startPoint,lineWidthNganDauTien);
        }


        // cac ngan tiep theo
        //create the path
        Integer soDayDan = isShowUkuleleChord == true ? 3 : 5;
        soNgan = isShowUkuleleChord == true ? 4 : 4;
        Float khoangCachGiuaCacDong = widthView / soDayDan.floatValue();
        for(Integer i = 0;i <= soDayDan; i++)
        {
            Float positionX = marginX + i.floatValue() * khoangCachGiuaCacDong;
            PointF fromPoint = new PointF(positionX, marginY);
            PointF startPoint = new PointF(positionX, heightView);
            this.drawLineCanvas(fromPoint,startPoint,lineWidthNganCacNganKhac);
        }

        Float khoangCachGiuaCacNgan = heightView / soNgan.floatValue();
        for(Integer i = 1; i < soNgan; i++)
        {
            Float positionY = i.floatValue() * khoangCachGiuaCacNgan;
            PointF fromPoint = new PointF(marginX, positionY + marginY);
            PointF startPoint = new PointF(widthView + marginX, positionY + marginY);
            this.drawLineCanvas(fromPoint,startPoint,lineWidthNganCacNganKhac);
        }


        // hien thi số thứ tự ngăn
        Integer hienThiViTriNgan = isShowUkuleleChord == true ? 1 : 2;
        Integer them1SoThuTuNganChoUkulele = isShowUkuleleChord == true ? 1 : 0;
        for(Integer i = 1;i <= soNgan + them1SoThuTuNganChoUkulele; i += hienThiViTriNgan)
        {
            Float positionY = i.floatValue() * khoangCachGiuaCacNgan + marginY;
            PointF point = new PointF(widthView + sizeOfPoint + marginX + marginTextFr, positionY - khoangCachGiuaCacNgan/2 + fontSize/2);
            String text = (viTriNgan + i) + " fr";
            this.drawTextCanvas(text,point,Color.BLACK);
        }


        // ngón bấm
        for (Integer index = 0; index < scheme.size(); index++)
        {
            Integer element = scheme.get(index);
            Float positionX = marginX + index.floatValue() * khoangCachGiuaCacDong - sizeOfPoint/2;
            if(element == -1 || element == 0)
            {
                String text = element == 0 ? "o" : "x";
                PointF point = new PointF(positionX + marginX/2 - fontSize/2, marginY/2 + fontSize/3);

                // draw text
                this.drawTextCanvas(text,point,Color.BLACK);
            }
            else if (element != 0 && element != -1)
            {
                Float positionY = (element - viTriNgan) * khoangCachGiuaCacNgan - (khoangCachGiuaCacNgan/2) - sizeOfPoint/2 + marginY;

                PointF point = new PointF(positionX + sizeOfPoint/2, positionY + sizeOfPoint/2);
                this.drawCircleCanvas(point,sizeOfPoint);

                // ngón tay bấm hợp âm
                if (fingers.length != 0 && index < fingers.length)
                {
                    String ngonBam = fingers[index];
                    if (ngonBam.length() != 0)
                    {
                        String text = ngonBam;
                        PointF pointViTriBam = new PointF(positionX + sizeOfPoint/2 - fontSize/3, positionY + sizeOfPoint/2 + fontSize/3);

                        // draw text
                        this.drawTextCanvas(text,pointViTriBam,Color.WHITE);
                    }

                }

            }

        }

    }

    private void drawLineCanvas(PointF fromPoint,PointF toPoint,Float lineWidth)
    {
        paint.setColor(Color.DKGRAY);
        paint.setStrokeWidth(lineWidth);
        canvas.drawLine(fromPoint.x, fromPoint.y, toPoint.x, toPoint.y, paint);
    }



    private void drawCircleCanvas(PointF point,Float  radius)
    {
        paint.setColor(Color.BLACK);
        canvas.drawCircle(point.x, point.y, radius, paint);
    }


    private void drawTextCanvas(String text,PointF point,int color)
    {
        paint.setColor(color);
        paint.setTextSize(fontSize);
        canvas.drawText(text, point.x, point.y, paint);
    }





    private ArrayList<Integer> analyticScheme()
    {
        String[] temp = this.chordGuitar.scheme.trim().split(" ");
        ArrayList<Integer> scheme = new ArrayList();
        for(String item : temp)
        {
            if(item.length() != 0)
            {
                if(item.toLowerCase().equals("x") == false)
                {
                    scheme.add(Integer.parseInt(item));
                }
                else
                {
                    scheme.add(-1);
                }
            }
        }

        return scheme;
    }


    private ArrayList<Integer> analyticSchemeUkulele()
    {
        String[] temp = this.chordUkulele.scheme.trim().split(" ");
        ArrayList<Integer> scheme = new ArrayList();
        int index = 1;
        for(String item : temp)
        {
            if(item.length() != 0)
            {
                if(item.toLowerCase().equals("x") == false)
                {
                    int number = Integer.parseInt(item);
                    number -= index * 10;
                    scheme.add(number);
                }
                else
                {
                    scheme.add(-1);
                }
            }

            index += 1;
        }

        return scheme;
    }


    private Integer timViTriNgan(ArrayList<Integer> scheme)
    {
        // tim vi tri ngan bam
        ArrayList<Integer> temp = new ArrayList();
        for(int i = 0; i < scheme.size(); i++)
        {
            Integer value = scheme.get(i);
            if(value > 0)
            {
                temp.add(value);
            }
        }

        Integer viTriNgan = 0;
        Integer viTriXaNhat = 0;
        if (temp.size() != 0)
        {
            viTriNgan = temp.get(0);
            viTriXaNhat = temp.get(0);
            for (Integer item : temp) {
                if(item < viTriNgan)
                {
                    viTriNgan = item;
                }

                if(item > viTriXaNhat)
                {
                    viTriXaNhat = item;
                }
            }
        }


        // cac ngan dau tien
        if(viTriXaNhat <= 4)
        {
            viTriNgan = 0;
        }
        else
        {
            if (viTriNgan > 0)
            {
                viTriNgan -= 1;
            }
        }


        return viTriNgan;
    }
}